# Teste Back-end Mawa Post 2021 #

Criar um painel administrativo para cotação de moedas utilizando [Laravel](https://laravel.com) e blade. No painel deverá ser possível fazer login e criar novas contas de usuário, além de poder realizar cotações da moeda escolhida e salvar o histórico das cotações realizadas pelo usuário logado.
Para fazer as cotações utilize a API [AwesomeAPI](https://docs.awesomeapi.com.br/api-de-moedas).

### O que será avaliado? ###

* Estruturação do código
* Documentação
* Funcionamento correto

### Diferenciais ###

* Docker
* UX das páginas do painel

### Opcional ###
Caso preferir, o candidato pode fazer a tarefa na forma de API Rest e utilizar algum framework de front-end, como Vue ou React JS.

### Envio ###

Enviar no e-mail dev@mawapost.com o link do fork do projeto com o assunto "Teste Mawa Back-end 2021 - Nome Candidato"

### Boa sorte! ###